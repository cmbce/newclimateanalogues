#!/usr/bin/env Rscript
########################
# 
# Script: autoScale.R
#
# Purpose: adjust the number of containers in docker-compose for ccexplorer
# Details: utilisation via crontab -e : 
#       */10 * * * * /home/cbarbu/newclimateanalogues/autoScale.R
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

# paramètres
workingFolder <- "/home/cbarbu/newclimateanalogues/"
setwd(workingFolder)
source("localParameters.R")

# importation des logs
matchString <- "^([^ ]+ +[^ ]+) +([^ ]+) +([^ ]+) +(.*)$"
textMessaging <- "/home/cbarbu/bin/telegramSendCorentin.py"

log <- readLines(logFile)

dat <- data.frame(date=gsub(matchString,"\\1",log),
                  # hour=gsub(matchString,"\\2",log),
                  container=gsub(matchString,"\\2",log),
                  session=gsub(matchString,"\\3",log),
                  message=gsub(" *","",gsub(matchString,"\\4",log)))

dat$dateTime <- as.POSIXct(dat$date,format="%Y-%m-%d %H:%M:%OS",tz="GMT")

#currently connected : start moins d'une heure et pas de stop 
iRecent <- which(Sys.time()-dat$dateTime < as.difftime(60,units="mins"))
lastCon <- dat[iRecent,]
lastStart <- lastCon[which(lastCon$message=="start"),"session"]
lastStop <- lastCon[which(lastCon$message=="stop"),"session"]
nCurrent <- length(setdiff(lastStart,lastStop))

conts <- system(paste0("docker ps | grep ",serviceName),intern=TRUE)
currentConts <- gsub("^([^ ]+) .*","\\1",conts)
usedConts <- unique(lastCon[which(lastCon$session %in% setdiff(lastStart,lastStop)),"container"])
currentEmpty <- setdiff(currentConts,usedConts)
# cat("currentEmpty:",currentEmpty,"\n")

# détermination du nombre de containers à viser
nContOpti <- ceiling(nCurrent/nViz)
nContAim <- min(max(nContOpti,nMinCont),nMaxCont)

nContCurrent <- length(conts)
bilan <- paste(monitorName,Sys.info()[["nodename"]],"n current sessions:",nCurrent,
               "; nContOpti:",nContOpti,"; nContAim:",nContAim,
               "; nContCurrent:",nContCurrent,"\n")

if(nContAim - nContCurrent >=1){
   # cmd <- paste0("docker-compose up --scale ",serviceName,"=",nContAim," -d")
   cmd <- paste0("docker service scale ",serviceName,"=",nContAim)
   # cat("Running:",cmd,"\n")
   system(cmd,intern=TRUE)
   cmd <- paste(textMessaging,"\"",bilan,monitorName,"up to",nContAim,"containers \n\"")   
   system(cmd)
}else if(nCurrent==0 && nContCurrent>nContAim){
# }else if(length(currentEmpty)>0 && nContCurrent>nContAim){
   # toBeStopped <- currentEmpty[1:(nContCurrent-nContAim)]  
   # cmd <- paste0("docker stop ",paste(toBeStopped,collapse=" ")) 
   cmd <- paste0("docker service scale ",serviceName,"=",nContAim)
   system(cmd,intern=TRUE)
   # cat("Running:",cmd,"\n")
   cmd <- paste(textMessaging,"\"",bilan,monitorName,"down to",nContAim,"containers \n\"")   
   system(cmd)
}
cmd <- paste(textMessaging,"\"",bilan,"\"")   
# system(cmd)

