
########################
# 
# Script:   getData.R
#
# Purpose:  get all bioc data from world clim (for the different models)
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

source("parameters.R")

downPath <- "http://biogeo.ucdavis.edu/data/worldclim/v2.1/fut/10m/"
# ssps <- c("126","245","370","585") 
# years <- c("2021-2040","2041-2060","2061-2080","2081-2100")
brokenLinks <- c()
brokenZip <- c()
for(model in models){
    for(ssp in choice_ssp){
        for(year in choice_year_span){
            fileName <-paste0(baseName,model,"_",ssp,"_",year) 
            tifFile <- file.path(tifFolder,paste0(fileName,".tif"))
            if(!file.exists(tifFile)){
                zipName <- paste0(fileName,".zip")
                # check if already downloaded
                zipFile <- file.path(zipFolder,zipName)
                if(file.exists(zipFile)){
                    cat(zipName,"OK\n")
                    out <- try(unzip(zipFile,exdir="Data/wcTif",junkpaths=TRUE))
                    if(length(out)==0){
                        cat("Broken zip:",zipName,"\n")
                        brokenZip <- c(brokenZip,zipName)
                    }else{
                        cat(tifFile,"OK\n")
                    }
                }else{
                    downFile <- paste0(downPath,zipName)
                    out <- try(download.file(url=downFile,destfile=zipFile))
                    if(class(out)=="try-error"){
                        brokenLinks <- c(brokenLinks,downFile)
                    }
                }
            }else{
                cat(tifFile,"OK\n")
            }
        }
    }
}
if(length(brokenLinks)>0){
    cat("Broken links:\n")
    print(brokenLinks)
}
if(length(brokenZip)>0){
    cat("Broken zips:\n")
    print(brokenZip)
}
