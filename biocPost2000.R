
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

library("terra")
library("predicts")
expected_years <- 2001:2020

# Note : compute the bioclim vars on averages of tmin-tmax-prec
#        should probably be computation first of bioclim per year, then use the average ?
#        but it would be much longer

# compute the bioclimatic indices for expected_years
initVars <- list()
for(type in c("prec","tmin","tmax")){
    patternFiles <- paste0("wc2.1_10m_",type,"_",min(expected_years),"-",max(expected_years),"_[0-9]+.tif")
    listFiles <- dir(path=file.path("Data","wcTif"),pattern=patternFiles,full.names=TRUE)
    initVars[[type]] <- rast(listFiles)
}
# next step is SLOW (several minutes)
cat("computing biovars\n")
bioc <- bcvars(initVars[["prec"]],
                initVars[["tmin"]],
                initVars[["tmax"]])
names(bioc) <- gsub("^bio([0-9]+)","wc2.1_10m_bio_\\1",names(bioc))

cat("checking the values\n")
file_path_1970_2000 <- "Data/wc2.1_10m_bio"
currentRef <- ImportLayers(file_path_1970_2000)
par(mfrow=c(4,5))
for(bioVar in 1:19){
    colName <- paste0("wc2.1_10m_bio_",bioVar)
    d1<-density(currentRef[[colName]],plot=FALSE)[[1]]
    d2 <-density(bioc[[colName]],plot=FALSE)[[1]]
    plot(d1,main=colName,ylim=c(0,max(c(d1$y,d2$y))))
    lines(d2,col="blue")
}
saveRDS(bioc,"recentHistorical.rds")
#=> weird as it is mostly very cold annual temperatures that increased, but nothing really weird
#   otherwise

