# README #

CC Explorer : explore climate analogues is a webapp available at https://www.ccexplorer.eu/. This is the code generating the webapp. 

### What is this repository for? ###

* WebApp allowing to visualize what a city will look like in a few decades with the global climatic change.
* Version 0.2

### Contribution guidelines ###
This project was started with little to no funding and help is welcome:

* Translations are welcome. you can add new translations or correct existing ones fairly easily by adding a language to translation/translation.json, then make a pull request. If you are not familiar with git and would like to help with translation, download the translation/translation.json file and try to translate. When you are done, contact us through the Issue tracker.
* Bug reports and feature requests are welcome in the issue tracker. Work on new features is even more welcome (just issue a pull request).

### Licence 
This project was originated by a French official research agency (INRAE), as such it is released under the 
[CeCILL-B Free Software License Agreement](https://cecill.info/licences/Licence_CeCILL-B_V1-en.html) of the [CeCILL license family](https://cecill.info/faq.en.html). This license has in particular a strong obligation of citation of the original work (here to cite this repository and the project coordinator: Corentin Barbu). See the text of these licenses in French and English in this folder.

### How do I get set up? ###
You need to get the Data/ folder from the developpers or download it yourself.

For WorldClim, this is a helper script: 

`source("getData.R")`

Then to prepare the data necessary for app.R, run in R, in this order:

* source("prepCityPoints.R") # select city points 
* source("unzip2000-2021.R") # unzip and rescale the recent historical data
* source("prepPost2000.R") # average the recent historical data
* source("biocPost2000.R") # computes bioclimatic variables on recent historical data
* source("prepHistoricalClimate.R") # prepare the historical climate data (1970-2000)
* source("prepCNRM-CM6-1.R") # prepares the future climate data according to CNRM-CM6-1
TODO: this should use the same data than prepAllModels.R
* source("prepAllModels.R")
* source("prepDisplay.R") # reshapes the raster for faster display
* source("app.R")
* UpdateTranslation() # met à jour si il y a eu des modifications dans translation/translation.json

You can choose to use the mean model or the CNRM-CM6-1 model by specifying the selected_model variable in parameters.R

To launch the app locally : 

* source("app.R")
* runApp("app.R")

### Setting up of a server
To launch the app on a server you need to set up shiny-server. 
shiny is the user serving the app.
Note, in case of issues, you can look at the files in /var/log/shiny-server/

To let the shiny server play nicely with nginx you might enjoy [this read](https://support.rstudio.com/hc/en-us/articles/213733868-Running-Shiny-Server-with-a-Proxy).

I personally create an other server in /etc/shiny-server/shiny-server.conf (see https://docs.rstudio.com/shiny-server/) and link directly to it from nginx.

### Deployment of the app
To test the bundle of the app:

source("testBareApp.R")

Push to the server (including data!) : 
* rsync -avz testDirBareApp/* shiny@vpsMoCoRiBA:/opt/shiny-server/climateanalogues/

Then on the server:  

* sudo service shiny-server restart

This is how I feed https://www.ccexplorer.eu/

To deploy on shinyApps.io, you can test the bundle of the app with:

source("testBareApp.R")

And then deploy with rsconnect:

rsconnect::deployApp(appDir=".",
appFiles=as.character(read.table("manifestShinyApps.io")$V1),
logLevel="verbose",appName="ccexplorer",appTitle="Climate Change Explorer",
account="climatechangeexplorer")

### Team ###

* Corentin M. Barbu is the project leader and main developper.
* Vasilis Çako was the initial main developper.

