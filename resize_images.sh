#!/bin/bash
#===============================================================================
#
#          FILE:  resize_images.sh
# 
#         USAGE:  ./resize_images.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  07/12/2020 21:33:37 CET
#      REVISION:  ---
#===============================================================================


convert -resize 800 images/help_desktop_fr.png www/help_desktop_fr.jpeg
convert -resize 800 images/help_desktop_en.png www/help_desktop_en.jpeg
convert -resize 800 images/help_desktop_es.png www/help_desktop_es.jpeg
convert -resize 800 images/days_deadly_threshold.png www/days_deadly_threshold.jpeg
convert images/logoAgroParisTech.png www/logoAgroParisTech.jpeg
convert images/logo_cea.png www/logo_cea.jpeg

